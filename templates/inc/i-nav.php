<button class="toggle-nav">
	<span>&nbsp;</span> Menu
</button>

<div class="nav">
	
	<div class="nav-wrap sw">
		<nav>
			<ul>
				<li class="nav-item">
					<a href="#" class="teamcanada-f ca-logo t-fa-abs">Home</a>
				</li>
				<li class="nav-item">
					<a href="#">LA 2015</a>
					<div class="subnav-dd">
						<ul>
							<li class="subnav-item"><a href="#">About LA 2015</a></li>
							<li class="subnav-item"><a href="#">Sports</a></li>
							<li class="subnav-item"><a href="#">Venues</a></li>
							<li class="subnav-item"><a href="#">Visitor Information</a></li>
						</ul>
						<div class="subnav-fact">
							<span class="t-fa fa-users">7,000 Athletes</span>
						</div><!-- .subnav-fact -->
					</div>
				</li>
				<li class="nav-item"><a href="#">Team Canada</a></li>
				<li class="nav-item"><a href="#">Highlights</a></li>
				<li class="nav-item"><a href="#">Results</a></li>
			</ul>
		</nav>

		<div class="meta-nav">
			<button class="t-fa-abs fa-search toggle-search">Toggle Search</button>
			<a href="#">Fr</a>
		</div>

	</div><!-- .nav-wrap -->

	
	<div class="nav-top">
		<div class="sw">

			<div class="links">
				<a href="#">Sponsors</a>
				<a href="#">Resources</a>
				<a href="#">Media</a>
				<a href="#">Contact</a>

				<?php include('i-social.php'); ?>
			</div><!-- .links -->

			<div class="logos">
				<a href="#" rel="external"><img src="../assets/dist/images/special-olympics-logo.svg" alt="Special Olympics Canada Logo"></a>
				<a href="#" rel="external"><img src="../assets/dist/images/special-olympics-world-games-la-2015-logo.svg" alt="Special Olympics World Games LA 2015 Logo"></a>
			</div><!-- .logos -->			
		
		</div><!-- .sw -->
	</div><!-- .nav-top -->
	
</div><!-- .nav -->