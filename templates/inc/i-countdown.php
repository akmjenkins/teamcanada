	<div class="countdown-wrap">
		<div class="countdown" data-target="2015-10-14">
		
			<span class="cd-label">
				Let the Games Begin In
			</span>
			
			<span class="countdown-blocks">
			
				<span class="days cd-block">
					<span class="val">114</span> <span class="t">days</span>
				</span>
				
				<span class="hours cd-block">
					<span class="val">03</span> <span class="t">hours</span>
				</span>
				
				<span class="minutes cd-block">
					<span class="val">46</span> <span class="t">minutes</span>
				</span>
				
				<span class="seconds cd-block">
					<span class="val">17</span> <span class="t">seconds</span>
				</span>
				
			</span><!-- .countdown-blocks -->
			
		</div><!-- .countdown -->
	</div><!-- .countdown-wrap -->
