			<footer>

				<div class="sw">
					<div class="footer-blocks">

						<div class="footer-block footer-nav-item">
							<h5><a href="#">LA 2015</a></h5>
							<ul>
								<li><a href="#">About LA 2015</a></li>
								<li><a href="#">Sports</a></li>
								<li><a href="#">Venues</a></li>
								<li><a href="#">Visitor Information</a></li>
							</ul>
						</div><!-- .footer-nav-item -->

						<div class="footer-block footer-nav-item">
							<h5><a href="#">Team Canada</a></h5>

							<ul class="footer-nav-two-col">
								<li><a href="#">Athletics</a></li>
								<li><a href="#">Gymnastics</a></li>
								<li><a href="#">Basketball</a></li>
								<li><a href="#">Swimming</a></li>
								<li><a href="#">Bocce</a></li>
								<li><a href="#">Powerlifting</a></li>
								<li><a href="#">10-Pin Bowling</a></li>
								<li><a href="#">Soccer</a></li>
								<li><a href="#">Golf</a></li>
								<li><a href="#">Softball</a></li>
							</ul>
						</div><!-- .footer-nav-item -->

						<div class="footer-block footer-nav-item">
							<h5><a href="#">Highlights</a></h5>

							<ul>
								<li><a href="#">Stories</a></li>
								<li><a href="#">Photos</a></li>
								<li><a href="#">Videos</a></li>
							</ul>
						</div><!-- .footer-nav-item -->

						<div class="footer-block">
						
							<h5>Get In Touch</h5>					
							<div class="footer-contact">	
								<address>
									21 St. Clair Avenue East, Suite 600 <br>
									Toronto ON <br>
									M4T 1L9
								</address>

								<div>
									<span class="block">TF 1 888 888 0608</span>
									<span class="block">Tel 1 416 927 9050</span>
									<span class="block">Fax 1 416 927 8475</span>
								</div>
							</div><!-- .footer-contact -->

							<h5>Connect</h5>
							<?php include('i-social.php'); ?>

						</div><!-- .footer-block -->
					</div><!-- .footer-blocks -->
				</div><!-- .sw -->

				<div class="copyright">
					<div class="sw">
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">Team Canada</a> All Rights Reserved.</li>
							<li><a href="#">Special Olympics Canada</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/dist/images/jac-logo.svg" alt="JAC Logo."></a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
				
			</footer><!-- footer -->

		</div><!-- .page-wrapper -->

		<?php include('i-search-form.php'); ?>

		<script>
			var templateJS = {
				templateURL: 'http://dev.me/teamcanada',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>

		<script src="../assets/dist/js/main.js"></script>
	</body>
</html>