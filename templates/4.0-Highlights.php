<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap d-bg">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-2.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>

			<div class="hero-content">

				<div class="hgroup">
					<h1 class="hgroup-title">Highlights</h1>
				</div><!-- .hgroup -->

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
					labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.
				</p>

			</div><!-- .hero-content -->

		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section class="dark-bg with-embellishment dark-overlay excerpt-section">
		<div class="lazybg overlaybg" data-src="../assets/dist/images/temp/full-bg.jpg"></div>
		<div class="sw">
			
			<div class="hgroup">
				<h4 class="hgroup-title">Latest Stories</h4>
				<span class="hr-embellish">
					<span></span>
				</span>
			</div><!-- .hgroup -->

			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
				Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.				
			</p>

		</div><!-- .sw -->
	</section><!-- .with-embellishment -->

	<section class="ov-grid two-up">

		<a class="ov-grid-item" href="#">
			
			<div class="article-content">
				
				<time pubdate datetime="2015-03-07" class="item-meta">March 7, 2015</time>
				<span class="hr-embellish"><span></span></span>					

				<span class="title">Lorem ipsum dolor sit amet consectetur adipisicing elit sed</span>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
				</p>

				<span class="button">Read More</span>

			</div><!-- .article-content -->

		</a><!-- .ov-grid-item -->

		<a class="ov-grid-item" href="#">
			
			<div class="article-content">
				
				<time pubdate datetime="2015-03-07" class="item-meta">March 7, 2015</time>
				<span class="hr-embellish"><span></span></span>					

				<span class="title">Lorem ipsum dolor sit amet consectetur adipisicing elit sed</span>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
				</p>

				<span class="button">Read More</span>

			</div><!-- .article-content -->

		</a><!-- .ov-grid-item -->

		<div class="ov-grid-item">
			<div class="pad-20 center">
				<a href="#" class="button">All Stories</a>
			</div><!-- .pad-40 -->
		</div><!-- .ov-grid-item -->

	</section><!-- .ov-grid -->

	<section class="dark-bg">
			
		<div class="hgroup section-title">
			<h4 class="hgroup-title">Latest Photos</h4>
			<span class="hr-embellish">
				<span></span>
			</span>
		</div><!-- .hgroup -->

		
		<div class="media-grid four-up d-bg">

			<div class="media-grid-item lazybg" data-src="../assets/dist/images/temp/media-1.jpg">
				<div class="media-grid-item-content">
					<div>
						<time class="item-meta" datetime="2015-03-07">March 7, 2015</time>
						<span class="hr-embellish"><span></span></span>

						<h4 class="media-grid-title">Photo Title 1</h4>
						<span class="mpopup media-grid-action photo" data-src="../assets/dist/images/temp/media-1.jpg" data-title="Photo Title 1" data-gallery="gallery_identifier"></span>

						<div class="share">
							Share:
							<a href="#" class="fb">Share on Facebook</a>
							<a href="#" class="tw">Share on Twitter</a>
						</div><!-- .share -->
					</div>
				</div><!-- .media-grid-item-content -->

			</div><!-- .media-grid-item -->

			<div class="media-grid-item lazybg" data-src="../assets/dist/images/temp/media-2.jpg" data-title="Photo Title 2" data-gallery="gallery_identifier">
				<div class="media-grid-item-content">
					<div>
						<time class="item-meta" datetime="2015-03-07">March 7, 2015</time>
						<span class="hr-embellish"><span></span></span>

						<h4 class="media-grid-title">Photo Title 2</h4>
						<span class="mpopup media-grid-action photo" data-src="../assets/dist/images/temp/media-2.jpg" data-title="Photo Title 2" data-gallery="gallery_identifier"></span>

						<div class="share">
							Share:
							<a href="#" class="fb">Share on Facebook</a>
							<a href="#" class="tw">Share on Twitter</a>
						</div><!-- .share -->
					</div>
				</div><!-- .media-grid-item-content -->

			</div><!-- .media-grid-item -->	

			<div class="media-grid-item lazybg" data-src="../assets/dist/images/temp/media-3.jpg" data-title="Photo Title 3" data-gallery="gallery_identifier">
				<div class="media-grid-item-content">
					<div>
						<time class="item-meta" datetime="2015-03-07">March 7, 2015</time>
						<span class="hr-embellish"><span></span></span>

						<h4 class="media-grid-title">Photo Title 3</h4>
						<span class="mpopup media-grid-action photo" data-src="../assets/dist/images/temp/media-3.jpg" data-title="Photo Title 3" data-gallery="gallery_identifier"></span>

						<div class="share">
							Share:
							<a href="#" class="fb">Share on Facebook</a>
							<a href="#" class="tw">Share on Twitter</a>
						</div><!-- .share -->
					</div>
				</div><!-- .media-grid-item-content -->

			</div><!-- .media-grid-item -->	

			<div class="media-grid-item lazybg" data-src="../assets/dist/images/temp/media-4.jpg" data-title="Photo Title 4" data-gallery="gallery_identifier">
				<div class="media-grid-item-content">
					<div>
						<time class="item-meta" datetime="2015-03-07">March 7, 2015</time>
						<span class="hr-embellish"><span></span></span>

						<h4 class="media-grid-title">Photo Title 4</h4>
						<span class="mpopup media-grid-action photo" data-src="../assets/dist/images/temp/media-4.jpg" data-title="Photo Title 4" data-gallery="gallery_identifier"></span>

						<div class="share">
							Share:
							<a href="#" class="fb">Share on Facebook</a>
							<a href="#" class="tw">Share on Twitter</a>
						</div><!-- .share -->
					</div>
				</div><!-- .media-grid-item-content -->

			</div><!-- .media-grid-item -->	

			<div class="media-grid-item lazybg" data-src="../assets/dist/images/temp/media-5.jpg" data-title="Photo Title 5" data-gallery="gallery_identifier">
				<div class="media-grid-item-content">
					<div>
						<time class="item-meta" datetime="2015-03-07">March 7, 2015</time>
						<span class="hr-embellish"><span></span></span>

						<h4 class="media-grid-title">Photo Title 5</h4>
						<span class="mpopup media-grid-action photo" data-src="../assets/dist/images/temp/media-5.jpg" data-title="Photo Title 5" data-gallery="gallery_identifier"></span>

						<div class="share">
							Share:
							<a href="#" class="fb">Share on Facebook</a>
							<a href="#" class="tw">Share on Twitter</a>
						</div><!-- .share -->
					</div>
				</div><!-- .media-grid-item-content -->

			</div><!-- .media-grid-item -->	

			<div class="media-grid-item lazybg" data-src="../assets/dist/images/temp/media-6.jpg" data-title="Photo Title 6" data-gallery="gallery_identifier">
				<div class="media-grid-item-content">
					<div>
						<time class="item-meta" datetime="2015-03-07">March 7, 2015</time>
						<span class="hr-embellish"><span></span></span>

						<h4 class="media-grid-title">Photo Title 6</h4>
						<span class="mpopup media-grid-action photo" data-src="../assets/dist/images/temp/media-6.jpg" data-title="Photo Title 6" data-gallery="gallery_identifier"></span>

						<div class="share">
							Share:
							<a href="#" class="fb">Share on Facebook</a>
							<a href="#" class="tw">Share on Twitter</a>
						</div><!-- .share -->
					</div>
				</div><!-- .media-grid-item-content -->

			</div><!-- .media-grid-item -->	

			<div class="media-grid-item lazybg" data-src="../assets/dist/images/temp/media-7.jpg" data-title="Photo Title 7" data-gallery="gallery_identifier">
				<div class="media-grid-item-content">
					<div>
						<time class="item-meta" datetime="2015-03-07">March 7, 2015</time>
						<span class="hr-embellish"><span></span></span>

						<h4 class="media-grid-title">Photo Title 7</h4>
						<span class="mpopup media-grid-action photo" data-src="../assets/dist/images/temp/media-7.jpg" data-title="Photo Title 7" data-gallery="gallery_identifier"></span>

						<div class="share">
							Share:
							<a href="#" class="fb">Share on Facebook</a>
							<a href="#" class="tw">Share on Twitter</a>
						</div><!-- .share -->
					</div>
				</div><!-- .media-grid-item-content -->

			</div><!-- .media-grid-item -->	

			<div class="media-grid-item lazybg" data-src="../assets/dist/images/temp/media-8.jpg" data-title="Photo Title 8" data-gallery="gallery_identifier">
				<div class="media-grid-item-content">
					<div>
						<time class="item-meta" datetime="2015-03-07">March 7, 2015</time>
						<span class="hr-embellish"><span></span></span>

						<h4 class="media-grid-title">Photo Title 8</h4>
						<span class="mpopup media-grid-action photo" data-src="../assets/dist/images/temp/media-8.jpg" data-title="Photo Title 8" data-gallery="gallery_identifier"></span>

						<div class="share">
							Share:
							<a href="#" class="fb">Share on Facebook</a>
							<a href="#" class="tw">Share on Twitter</a>
						</div><!-- .share -->
					</div>
				</div><!-- .media-grid-item-content -->

			</div><!-- .media-grid-item -->										

		</div><!-- .four-up -->

	</section><!-- .dark-bg -->

	<section class="nopad">

		<div class="hgroup section-title">
			<h4 class="hgroup-title">Latest Video</h4>
			<span class="hr-embellish">
				<span></span>
			</span>
		</div><!-- .hgroup -->

		<div class="inline-video with-overlay dark-bg bounce mpopup" data-src="https://www.youtube.com/watch?v=M-wDSryXqFU">
			<div class="lazybg overlaybg img" data-src="../assets/dist/images/temp/video-bg.jpg"></div>

			<div class="sw inline-video-content">

				<span class="h1-style">Video Title</span>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
					labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.
				</p>

			</div><!-- .inline-video-content -->

		</div><!-- .inline-video -->
		
	</section>


</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>