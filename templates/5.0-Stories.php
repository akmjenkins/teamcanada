<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap d-bg">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-2.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>

			<div class="hero-content">


				<div class="hgroup">
					<h1 class="hgroup-title">Stories</h1>
				</div><!-- .hgroup -->

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
					labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.
				</p>

			</div><!-- .hero-content -->

		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section class="nopad medium-bg">
		
		<div class="hgroup section-title">
			<h4 class="hgroup-title">Featured Story</h4>
			<span class="hr-embellish">
				<span></span>
			</span>
		</div><!-- .hgroup -->

		<div class="ov-grid two-up no-grow">

			<div class="ov-grid-item lazybg no-collapse" data-src="../assets/dist/images/temp/story-1.jpg"></div>

			<a href="#" class="ov-grid-item">
				
					<div class="article-content">
						
						<time pubdate datetime="2015-03-07" class="item-meta">March 7, 2015</time>
						<span class="hr-embellish"><span></span></span>					

						<span class="title">Lorem ipsum dolor sit amet consectetur adipisicing elit sed</span>

						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
							Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
						</p>

						<span class="button">Read More</span>

					</div><!-- .article-content -->
			</a><!-- .ov-grid-item -->	

		</div><!-- .ov-grid -->			

	</section><!-- .nopad -->

	<section class="filter-section">

		<div class="filter-bar">
			<div class="sw">
				<div class="filter-bar-content">				
					<div class="filter-bar-left">
						<div class="count">
							<span class="num">30</span> Photos Found
						</div><!-- .count -->
					</div><!-- .filter-bar-left -->
					
					<div class="filter-bar-meta">
					
						<div class="filter-controls">
							<button class="previous">Prev</button>
							<button class="next">Next</button>
						</div><!-- .filter-controls -->
				
					</div><!-- .filter-bar-meta -->
				</div><!-- .filter-bar-content -->
			</div><!-- .sw -->
		</div><!-- .filter-bar -->

		<div class="filter-content">	

			<div class="ov-grid two-up no-grow">

				<a href="#" class="ov-grid-item">
					
						<div class="article-content">
							
							<time pubdate datetime="2015-03-07" class="item-meta">March 7, 2015</time>
							<span class="hr-embellish"><span></span></span>					

							<span class="title">Lorem ipsum dolor sit amet consectetur adipisicing elit sed</span>

							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
								Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
							</p>

							<span class="button">Read More</span>

						</div><!-- .article-content -->
				</a><!-- .ov-grid-item -->

				<a href="#" class="ov-grid-item">
					
						<div class="article-content">
							
							<time pubdate datetime="2015-03-07" class="item-meta">March 7, 2015</time>
							<span class="hr-embellish"><span></span></span>					

							<span class="title">Lorem ipsum dolor sit amet consectetur adipisicing elit sed</span>

							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
								Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
							</p>

							<span class="button">Read More</span>

						</div><!-- .article-content -->
				</a><!-- .ov-grid-item -->	

				<a href="#" class="ov-grid-item">
					
						<div class="article-content">
							
							<time pubdate datetime="2015-03-07" class="item-meta">March 7, 2015</time>
							<span class="hr-embellish"><span></span></span>					

							<span class="title">Lorem ipsum dolor sit amet consectetur adipisicing elit sed</span>

							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
								Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
							</p>

							<span class="button">Read More</span>

						</div><!-- .article-content -->
				</a><!-- .ov-grid-item -->

				<a href="#" class="ov-grid-item">
					
						<div class="article-content">
							
							<time pubdate datetime="2015-03-07" class="item-meta">March 7, 2015</time>
							<span class="hr-embellish"><span></span></span>					

							<span class="title">Lorem ipsum dolor sit amet consectetur adipisicing elit sed</span>

							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
								Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
							</p>

							<span class="button">Read More</span>

						</div><!-- .article-content -->
				</a><!-- .ov-grid-item -->					

			</div><!-- .ov-grid -->			

		</div><!-- .filter-content -->

	</section><!-- .filter-section -->


</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>