<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap d-bg">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-2.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>

			<div class="hero-content">


				<div class="hgroup">
					<h1 class="hgroup-title">LA 2015</h1>
				</div><!-- .hgroup -->

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
					labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.
				</p>

			</div><!-- .hero-content -->

		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section class="ov-grid two-up retain-all">
		<a class="ov-grid-item bounce" href="#">

			<div class="img-wrap">
				<div class="lazybg img" data-src="../assets/dist/images/temp/media-1.jpg"></div>
			</div><!-- .img-wrap -->

			<div class="ov-grid-item-content">			

				<div class="hgroup">
					<div class="h4-style hgroup-title">About LA 2015</div>
					<span class="hr-embellish">
						<span></span>
					</span>				
				</div>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.				
				</p>

				<span class="button">Read More</span>

			</div><!-- .ov-grid-item-content -->			

		</a><!-- .ov-grid-item -->

		<a class="ov-grid-item bounce" href="#">

			<div class="img-wrap">
				<div class="lazybg img" data-src="../assets/dist/images/temp/media-2.jpg"></div>
			</div><!-- .img-wrap -->

			<div class="ov-grid-item-content">			

				<div class="hgroup">
					<div class="h4-style hgroup-title">Sports</div>
					<span class="hr-embellish">
						<span></span>
					</span>				
				</div>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.				
				</p>

				<span class="button">Read More</span>

			</div><!-- .ov-grid-item-content -->			

		</a><!-- .ov-grid-item -->

		<a class="ov-grid-item bounce" href="#">

			<div class="img-wrap">
				<div class="lazybg img" data-src="../assets/dist/images/temp/media-3.jpg"></div>
			</div><!-- .img-wrap -->

			<div class="ov-grid-item-content">

				<div class="hgroup">
					<div class="h4-style hgroup-title">Venues</div>
					<span class="hr-embellish">
						<span></span>
					</span>				
				</div>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.				
				</p>

				<span class="button">Read More</span>

			</div><!-- .ov-grid-item-content -->

		</a><!-- .ov-grid-item -->	

		<a class="ov-grid-item bounce" href="#">

			<div class="img-wrap">
				<div class="lazybg img" data-src="../assets/dist/images/temp/media-5.jpg"></div>
			</div><!-- .img-wrap -->

			<div class="ov-grid-item-content">			

				<div class="hgroup">
					<div class="h4-style hgroup-title">Visitor Information</div>
					<span class="hr-embellish">
						<span></span>
					</span>				
				</div>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.				
				</p>

				<span class="button">Read More</span>

			</div><!-- .ov-grid-item-content -->			

		</a><!-- .ov-grid-item -->			

	</section>


</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>