<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap d-bg">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-2.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>

			<div class="hero-content">


				<div class="hgroup">
					<h1 class="hgroup-title">Contact</h1>
				</div><!-- .hgroup -->

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
					labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.
				</p>

			</div><!-- .hero-content -->

		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>	
		<div class="sw">

			<div class="center">

				<div class="hgroup">
					<h4 class="hgroup-title">Media Contact</h4>
					<span class="hr-embellish"><span></span></span>
				</div><!-- .hgroup -->		

				<div class="hgroup">
					<h5 class="hgroup-title">Allison Love</h5>
					<span>Communications Officer</span>
				</div><!-- .hgroup -->

				<span class="block">1.888.888.0608, ext. 4312 (toll-free)</span>
				<span class="block">416.927.9050, ext. 4312</span>

				<br>
				<br>

				<p class="excerpt">Fill out the form below to get in touch with Allison.</p>				

			</div><!-- .center -->		

			<form action="/" class="body-form full">
				<div class="fieldset grid collapse-700">

					<div class="col-2 col">
						<div class="item"><input name="fname" type="text" placeholder="First Name"></div>
					</div><!-- .col -->

					<div class="col-2 col">
						<div class="item"><input name="lname" type="text" placeholder="Last Name"></div>
					</div><!-- .col -->					

					<div class="col-2 col">
						<div class="item"><input name="email" type="email" placeholder="E-mail Address"></div>
					</div><!-- .col -->					

					<div class="col-2 col">
						<div class="item"><input name="phone" type="tel" placeholder="Phone Number"></div>
					</div><!-- .col -->		

					<div class="col col-1">
						<div class="item"><textarea name="message" placeholder="Message"></textarea></div>
					</div>			

				</div><!-- .grid -->
				<button class="button" type="submit">Send Message</button>				
			</form><!-- .body-form -->

		</div><!-- .sw -->
	</section>


</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>