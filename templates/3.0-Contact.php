<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap d-bg">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-2.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>

			<div class="hero-content">


				<div class="hgroup">
					<h1 class="hgroup-title">Contact</h1>
				</div><!-- .hgroup -->

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
					labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.
				</p>

			</div><!-- .hero-content -->

		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>	
		<div class="sw">
			
			<p class="center excerpt">
				Fill out the form below to get in touch with us. Or, you can call us toll free at <span class="primary">+1 888 123 4567</span>.
			</p>

			<form action="/" class="body-form full">
				<div class="fieldset grid collapse-700">

					<div class="col-2 col">
						<div class="item"><input name="fname" type="text" placeholder="First Name"></div>
					</div><!-- .col -->

					<div class="col-2 col">
						<div class="item"><input name="lname" type="text" placeholder="Last Name"></div>
					</div><!-- .col -->					

					<div class="col-2 col">
						<div class="item"><input name="email" type="email" placeholder="E-mail Address"></div>
					</div><!-- .col -->					

					<div class="col-2 col">
						<div class="item"><input name="phone" type="tel" placeholder="Phone Number"></div>
					</div><!-- .col -->		

					<div class="col col-1">
						<div class="item"><textarea name="message" placeholder="Message"></textarea></div>
					</div>			

				</div><!-- .grid -->
					<button class="button" type="submit">Send Message</button>				
			</form><!-- .body-form -->

		</div><!-- .sw -->
	</section>

	<section class="dark-bg nopad">
		<div class="pad-20 center">
			<div class="hgroup">
				<h4 class="hgroup-title">Our Location</h4>
				<span class="hr-embellish"><span></span></span>		
			</div><!-- .hgroup -->
		</div><!-- .pad-20.center -->


		<div class="gmap">
			<div 
				class="map" 
				data-center="43.6877961,-79.3927439" 
				data-zoom="17"
				data-markers='[{"position":"43.6880961,-79.3927439","htmlmarker":"tc-map-marker","title":"Special Olympics"}]'>
			</div><!-- .map -->


			<div class="address-overlay-wrap">
				<div class="address-overlay">
					<address>
						21 St. Clair Avenue East, Suite 6000 <br>
						Toronto, ON M4T 1L9
					</address>

					<span class="block">TF 1 888 888 0608</span>
					<span class="block">PH 1 416-927-9050</span>
					<span class="block">FX 1 416-927-8475</span>
				</div><!-- .address-overlay -->
			</div><!-- .address-overlay-wrap -->

		</div><!-- .gmap -->		

	</section><!-- .dark-bg -->


</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>