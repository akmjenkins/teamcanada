<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap d-bg">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-2.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>

			<div class="hero-content">

				<div class="hgroup">
					<h1 class="hgroup-title">Resources</h1>
				</div><!-- .hgroup -->

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor 
					incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis 
					nostrud exercitation ullamco laboris.
				</p>

			</div><!-- .hero-content -->

		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section class="filter-section">

		<div class="filter-bar">
			<div class="sw">
				<div class="filter-bar-content">				
					<div class="filter-bar-left">
						<div class="count">
							<span class="num">50</span> Resources Found
						</div><!-- .count -->
					</div><!-- .filter-bar-left -->
					
					<div class="filter-bar-meta">

						<div class="selector with-arrow">
							<select name="sort">
								<option>Sort</option>
								<option value="1">Date</option>
							</select>
							<span class="value"></span>
						</div><!-- .selector -->

						<form action="" class="single-form">
							<div class="fieldset">
								<input type="text" name="s" placeholder="Search for a resource...">
								<button class="t-fa-abs fa-search">Search</button>
							</div><!-- .fieldset -->
						</form><!-- .single-form -->
					
						<div class="filter-controls">
							<button class="previous">Prev</button>
							<button class="next">Next</button>
						</div><!-- .filter-controls -->
				
					</div><!-- .filter-bar-meta -->
				</div><!-- .filter-bar-content -->
			</div><!-- .sw -->
		</div><!-- .filter-bar -->

		<div class="filter-content">
			
			<div class="ov-grid three-up">

				<a href="#" class="ov-grid-item">
					<div class="article-content">
						
						<span class="item-meta">Document</span>
						<span class="hr-embellish"><span></span></span>
						<span class="title">Title of Resource</span>
						<span class="button">Read More</span>

					</div><!-- .article-content -->
				</a><!-- .ov-grid-item -->

				<a href="#" class="ov-grid-item">
					<div class="article-content">
						
						<span class="item-meta">PDF</span>
						<span class="hr-embellish"><span></span></span>
						<span class="title">Title of Resource</span>
						<span class="button">Read More</span>

					</div><!-- .article-content -->
				</a><!-- .ov-grid-item -->

				<a href="#" class="ov-grid-item">
					<div class="article-content">
						
						<span class="item-meta">Document</span>
						<span class="hr-embellish"><span></span></span>
						<span class="title">Title of Resource</span>
						<span class="button">Read More</span>

					</div><!-- .article-content -->
				</a><!-- .ov-grid-item -->

				<a href="#" class="ov-grid-item">
					<div class="article-content">
						
						<span class="item-meta">PDF</span>
						<span class="hr-embellish"><span></span></span>
						<span class="title">Title of Resource</span>
						<span class="button">Read More</span>

					</div><!-- .article-content -->
				</a><!-- .ov-grid-item -->	

				<a href="#" class="ov-grid-item">
					<div class="article-content">
						
						<span class="item-meta">Document</span>
						<span class="hr-embellish"><span></span></span>
						<span class="title">Title of Resource</span>
						<span class="button">Read More</span>

					</div><!-- .article-content -->
				</a><!-- .ov-grid-item -->

				<a href="#" class="ov-grid-item">
					<div class="article-content">
						
						<span class="item-meta">PDF</span>
						<span class="hr-embellish"><span></span></span>
						<span class="title">Title of Resource</span>
						<span class="button">Read More</span>

					</div><!-- .article-content -->
				</a><!-- .ov-grid-item -->					


			</div><!-- .ov-grid -->

		</div><!-- .filter-content -->

	</section><!-- .filter-section -->


</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>