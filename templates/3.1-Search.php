<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap d-bg">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-2.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>

			<div class="hero-content">

				<div class="hgroup">
					<h1 class="hgroup-title">Search Results</h1>
				</div><!-- .hgroup -->

				<p>
					Your results for "Query"
				</p>

			</div><!-- .hero-content -->

		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section class="filter-section">

		<div class="hgroup section-title">
			<h4 class="hgroup-title">4 Results in Stories</h4>
			<span class="hr-embellish"><span></span></span>
		</div><!-- .hgroup -->	

		<div class="filter-bar">
			<div class="sw">
				<div class="filter-bar-content">				
					<div class="filter-bar-left">
						<div class="count">
							<span class="num">4</span> Stories Found
						</div><!-- .count -->
					</div><!-- .filter-bar-left -->
					
					<div class="filter-bar-meta">
					
						<div class="filter-controls">
							<button class="previous">Prev</button>
							<button class="next">Next</button>
						</div><!-- .filter-controls -->
				
					</div><!-- .filter-bar-meta -->
				</div><!-- .filter-bar-content -->
			</div><!-- .sw -->
		</div><!-- .filter-bar -->

		<div class="filter-content">
			
			<div class="ov-grid two-up no-grow">

				<a href="#" class="ov-grid-item">
					
						<div class="article-content">
							
							<time pubdate datetime="2015-03-07" class="item-meta">March 7, 2015</time>
							<span class="hr-embellish"><span></span></span>					

							<span class="title">Lorem ipsum dolor sit amet consectetur adipisicing elit sed</span>

							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
								Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
							</p>

							<span class="button">Read More</span>

						</div><!-- .article-content -->
				</a><!-- .ov-grid-item -->

				<a href="#" class="ov-grid-item">
					
						<div class="article-content">
							
							<time pubdate datetime="2015-03-07" class="item-meta">March 7, 2015</time>
							<span class="hr-embellish"><span></span></span>					

							<span class="title">Lorem ipsum dolor sit amet consectetur adipisicing elit sed</span>

							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
								Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
							</p>

							<span class="button">Read More</span>

						</div><!-- .article-content -->
				</a><!-- .ov-grid-item -->	

			</div><!-- .ov-grid -->

		</div><!-- .filter-content -->

	</section><!-- .filter-section -->

	<section class="filter-section">

		<div class="hgroup section-title">
			<h4 class="hgroup-title">1 Page Found</h4>
			<span class="hr-embellish"><span></span></span>
		</div><!-- .hgroup -->		

		<div class="filter-bar">
			<div class="sw">
				<div class="filter-bar-content">				
					<div class="filter-bar-left">
						<div class="count">
							<span class="num">1</span> Page Found
						</div><!-- .count -->
					</div><!-- .filter-bar-left -->
					
					<div class="filter-bar-meta">
					
						<div class="filter-controls">
							<button class="previous">Prev</button>
							<button class="next">Next</button>
						</div><!-- .filter-controls -->
				
					</div><!-- .filter-bar-meta -->
				</div><!-- .filter-bar-content -->
			</div><!-- .sw -->
		</div><!-- .filter-bar -->

		<div class="filter-content">
			
			<div class="ov-grid two-up no-grow">

				<a href="#" class="ov-grid-item">
					
						<div class="article-content">
							
							<time pubdate datetime="2015-03-07" class="item-meta">March 7, 2015</time>
							<span class="hr-embellish"><span></span></span>					

							<span class="title">Lorem ipsum dolor sit amet consectetur adipisicing elit sed</span>

							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
								Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
							</p>

							<span class="button">Read More</span>

						</div><!-- .article-content -->

				</a><!-- .ov-grid-item -->

			</div><!-- .ov-grid -->

		</div><!-- .filter-content -->

	</section><!-- .filter-section -->


</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>