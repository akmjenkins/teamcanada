<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap d-bg">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-1.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>

			<div class="hero-content">

				<span class="h1-style">July 25 - August 2, 2015</span>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
					labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.
				</p>

				<div class="buttons">
					<a href="#" class="button">Find Out More</a>
					<a href="#" class="button">Schedule</a>
				</div>

			</div><!-- .hero-content -->

		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">
			
			<div class="center">
				<h3>Let The Games Begin In</h3>
				<div class="countdown" data-target="2015-07-25"></div>
			</div><!-- .center -->

		</div><!-- .sw -->
	</section>

	<section class="patterned-bg">
		<div class="sw">
			
			<div class="hgroup center">
				<h4 class="hgroup-title">Fast Facts</h4>
				<span class="hr-embellish">
					<span></span>
				</span>
			</div><!-- .hgroup -->


			<div class="fast-facts">

				<div class="fast-fact">
					<span class="t-fa fa-calendar ff-text">When</span>
					<span class="ff-tip">July 25 - August 2, 2015</span>
				</div><!-- .fast-fact -->

				<div class="fast-fact">
					<span class="t-fa fa-map-marker ff-text">Where</span>
					<span class="ff-tip">Los Angeles, California</span>
				</div><!-- .fast-fact -->				

				<div class="fast-fact">
					<span class="t-fa fa-users ff-text">Participants</span>
					<span class="ff-tip">7,000 Athletes</span>
				</div><!-- .fast-fact -->	

				<div class="fast-fact">
					<span class="teamcanada-f ca-logo ff-text">Team Canada</span>
					<span class="ff-tip">164 Athletes</span>
				</div><!-- .fast-fact -->			

			</div><!-- .fast-fact -->

		</div><!-- .sw -->
	</section><!-- .patterned-bg -->

	<section class="dark-bg with-embellishment with-overlay excerpt-section">
		<div class="lazybg overlaybg" data-src="../assets/dist/images/temp/full-bg.jpg"></div>
		<div class="sw">
			
			<div class="hgroup">
				<h4 class="hgroup-title">Latest Stories</h4>
				<span class="hr-embellish">
					<span></span>
				</span>
			</div><!-- .hgroup -->

			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
				Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.				
			</p>

		</div><!-- .sw -->
	</section><!-- .with-embellishment -->

	<section class="ov-grid two-up">

		<a class="ov-grid-item" href="#">
			
			<div class="article-content">
				
				<time pubdate datetime="2015-03-07" class="item-meta">March 7, 2015</time>
				<span class="hr-embellish"><span></span></span>					

				<span class="title">Lorem ipsum dolor sit amet consectetur adipisicing elit sed</span>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
				</p>

				<span class="button">Read More</span>

			</div><!-- .article-content -->

		</a><!-- .ov-grid-item -->

		<a class="ov-grid-item" href="#">
			
			<div class="article-content">
				
				<time pubdate datetime="2015-03-07" class="item-meta">March 7, 2015</time>
				<span class="hr-embellish"><span></span></span>					

				<span class="title">Lorem ipsum dolor sit amet consectetur adipisicing elit sed</span>

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
					Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
				</p>

				<span class="button">Read More</span>

			</div><!-- .article-content -->

		</a><!-- .ov-grid-item -->

	</section><!-- .ov-grid -->


</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>