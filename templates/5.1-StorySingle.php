<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap d-bg">

	<div class="big-fader fader">
		<div class="fader-item">
			<div class="fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-2.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>

			<div class="hero-content">


				<div class="hgroup">
					<h1 class="hgroup-title">Stories Single Post</h1>
				</div><!-- .hgroup -->

				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
					labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.
				</p>

			</div><!-- .hero-content -->

		</div><!-- .fader-item -->
	</div><!-- .fader -->
	
</div><!-- .hero -->

<div class="body">

	<section>
		<div class="sw">

			<article>

				<div class="article-pub-date">
					<time pubdate datetime="2015-03-07" class="item-meta">March 7, 2015</time>
					<span class="hr-embellish"><span></span></span>	
				</div><!-- .article-pub-date -->

				<div class="main-body">				
					<div class="content">

						<div class="article-body">
							<p>
								Donec bibendum enim in velit malesuada mollis. Etiam vitae dapibus ipsum. Donec at porta massa. Duis porttitor 
								porta maximus. Duis ut metus quis nisi hendrerit lacinia. Nunc hendrerit, nulla eu posuere aliquam, neque turpis 
								sodales urna, a interdum leo nunc eu nisl. Nunc pretium, diam sit amet dignissim mattis, enim magna ornare eros, 
								sed lacinia lorem urna non neque. In posuere nulla imperdiet risus laoreet porta. Sed egestas iaculis congue. 
								Praesent hendrerit tellus diam. Maecenas eget elit sed purus iaculis lacinia. Integer vel ipsum mauris.
							</p>

							<p>
								Vestibulum ut velit eros. Aenean diam eros, finibus finibus aliquet non, consequat id nunc. Vivamus viverra mi dui, 
								vitae porta risus aliquam quis. Nulla pellentesque porttitor eleifend. Cras quam lectus, pretium eu magna sed, 
								blandit laoreet urna. Vivamus imperdiet dignissim leo, vitae mollis ipsum imperdiet in. Quisque nunc quam, cursus 
								sit amet molestie in, dignissim ut felis. Suspendisse potenti. Mauris facilisis, tellus a congue malesuada, lectus 
								libero egestas arcu, dapibus auctor felis ipsum eget dui.
							</p>

							<p>
								Nam vehicula, felis sit amet luctus volutpat, enim ante blandit mi, sit amet pulvinar nibh lacus ut leo. Sed 
								tristique pretium dapibus. Donec at gravida nibh, in scelerisque ipsum. Fusce pharetra malesuada convallis. Cras 
								purus mi, gravida et consequat eu, euismod in orci. In luctus vitae dui consequat tempus. Aenean ultrices tempor 
								ullamcorper. Praesent nec enim rhoncus, sagittis lorem in, consectetur lectus. Praesent in pellentesque purus. 
								Mauris venenatis massa et sapien vestibulum consectetur.
							</p>

						</div><!-- .article-body -->
						
					</div><!-- .content -->


					<aside class="sidebar sidebar-primary">
						<div class="sidebar-mod acc-mod">
							<h5 class="mod-title">Archives</h5>

							<div class="acc with-indicators">

								<div class="acc-item">
									<div class="acc-item-handle">2015 (13)</div>
									<div class="acc-item-content">
										
										<ul>
											<li><a href="#">March (3)</a></li>
											<li><a href="#">February (5)</a></li>
											<li><a href="#">January (5)</a></li>
										</ul>

									</div><!-- .acc-item-content -->
								</div><!-- .acc-item -->

								<div class="acc-item">
									<div class="acc-item-handle">2014 (25)</div>
									<div class="acc-item-content">
										
										<ul>
											<li><a href="#">March (3)</a></li>
											<li><a href="#">February (5)</a></li>
											<li><a href="#">January (5)</a></li>
										</ul>

									</div><!-- .acc-item-content -->
								</div><!-- .acc-item -->

								<div class="acc-item">
									<div class="acc-item-handle">2013 (30)</div>
									<div class="acc-item-content">
										
										<ul>
											<li><a href="#">March (3)</a></li>
											<li><a href="#">February (5)</a></li>
											<li><a href="#">January (5)</a></li>
										</ul>

									</div><!-- .acc-item-content -->
								</div><!-- .acc-item -->								

							</div><!-- .acc -->

						</div>
					</aside><!-- .sidebar -->

					<aside class="sidebar sidebar-secondary">
						<div class="sidebar-mod share-mod">
							<h5 class="mod-title">Share</h5>

							<a href="#" class="share-fb">Facebook</a>
							<a href="#" class="share-tw">Twitter</a>

						</div><!-- .share-mod -->
					</aside><!-- .sidebar -->



				</div><!-- .main-body -->
			</article>

		</div><!-- .sw -->
	</section>


</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>