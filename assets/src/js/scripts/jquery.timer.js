;(function(context) {

	var timeStrings = {
		days: "days",
		minutes: "minutes",
		hours: "hours",
		seconds: "seconds"
	};

	require('../../../bower_components/circles/circles.js');

	$.fn.countdown = function( action, options) {

		return this.each( function ( i, element ) {
			var 
				el = $(element),
				timer = el.data('timer');
				
			if ( action === undefined || action == 'init' ) {
				el.html('<div class="days"></div><div class="hours"></div><div class="minutes"></div><div class="seconds"></div>');
				timer = new Timer(el);
				timer.start();
				el.data('timer',timer);
				
			} else if(typeof timer[ action ] === 'function') {
				timer[ action ]( options )
			}
		});
		
	};

	var
		ms = 1,
		s = ms*1000,
		m = s*60,
		h = m*60,
		d = h*24,
		randId = function() { 
			return Math.random().toString(36).substring(2,10) 
		},
		pad = function(num,len,padWith) {
		
			var strNum = num+'';
			while(strNum.length < len) {
				strNum = (padWith+'')+strNum;
			}
		
			return strNum;
		};

	var Timer = function(el) {
		var self = this;
		this.ran = false;
		this.interval = null;
		this.target = new Date(el.data('target').valueOf());
		this.el = el;

		this.circles = {
			days:{
				el: el.find('.days'),
				max: 365,
				circle: null
			},
			hours:{
				el: el.find('.hours'),
				max: 24,
				circle: null
			},
			minutes:{
				el: el.find('.minutes'),
				max: 60,
				circle: null
			},			
			seconds:{
				el: el.find('.seconds'),
				max: 60,
				circle: null
			},
		};

		$.each(this.circles,function(key,obj) {
			var id = randId();
			obj.el.attr('id',id);

			obj.el.attr('data-interval',timeStrings[key]);

			obj.circle = Circles.create({
				id:id,
				maxValue:obj.max,
				width:6,
				radius:obj.el.width()/2,
				duration:400,
				colors:['#D8D8D8','#B12335']
			});
		});
	};

	Timer.prototype.start = function() {
		var self = this;
		
		//start immediately
		self.display();
		
		this.interval = setInterval(function() {
			self.display();
		},1000);

	};

	Timer.prototype.display = function() {

		var 
			days,
			hours,
			minutes,
			seconds,
			milliseconds,
			now = new Date().valueOf(),
			duration = this.target-now;
			
			days = ( Math.floor( duration/d ) ).toFixed(0);
				duration -= days*d
			hours = ( Math.floor( duration/h ) ).toFixed(0);
				duration -= hours*h;
			minutes = ( Math.floor( duration/m ) ).toFixed(0);
				duration -= minutes*m;
			seconds = ( Math.floor( duration/s ) ).toFixed(0);
				duration -= seconds*s;
			milliseconds = duration;

			this.circles.days.circle.update(+days,this.ran ? 0 : undefined);
			this.circles.hours.circle.update(+hours,this.ran ? 0 : undefined);
			this.circles.minutes.circle.update(+minutes,this.ran ? 0 : undefined);
			this.circles.seconds.circle.update(+seconds,this.ran ? 0 : undefined);

			if(!this.ran) {
				this.circles.days.circle.update(true);
				this.circles.hours.circle.update(true);
				this.circles.minutes.circle.update(true);
				this.circles.seconds.circle.update(true);
			}
			


			this.ran = true;

	};

	Timer.prototype.stop = function() {
		clearInterval(this.interval);
	};

}(typeof ns !== 'undefined' ? window[ns] : undefined));